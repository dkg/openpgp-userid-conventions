---
docname: draft-dkg-openpgp-userid-conventions-01
cat: info
submissiontype: IETF
ipr: trust200902
lang: en
title: OpenPGP User ID Conventions
area: Security
wg: openpgp
kw: OpenPGP, userid
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/dkg/openpgp-userid-conventions"
  latest: "https://dkg.gitlab.io/openpgp-userid-conventions/"
author:
- ins: D. K. Gillmor
  name: Daniel Kahn Gillmor
  org: ACLU
  email: dkg@fifthhorseman.net
--- abstract

OpenPGP User IDs are UTF-8 strings.
Existing documents claim that by convention, they contain "an RFC 2822 name-addr object", but that's not the case.
This document attempts to better describe the actual conventions about User IDs in the deployed OpenPGP ecosystem.

--- middle

# Introduction

OpenPGP certificates contain User IDs.
An OpenPGP User ID packet contains a simple UTF-8 string.
According to {{!RFC4880}} and its successor {{!I-D.ietf-openpgp-crypto-refresh}}:

> By convention, it includes an {{?RFC2822}} mail name-addr

But in practice, this is not what most OpenPGP implementations generate or expect.
This document tries to better describe the actual convention used.

## Terminology

The term "OpenPGP Certificate" is used in this document interchangeably with "OpenPGP Transferable Public Key", as defined in {{Section 10.1 of !I-D.ietf-openpgp-crypto-refresh}}.

{::boilerplate bcp14-tagged}

# OpenPGP User ID Conventions {#uid-conventions}

An OpenPGP User ID has no formal constraints other than being a UTF-8 string, but common conventions govern its use in specific contexts.

In the context of sending and receiving signed and encrypted e-mail messages, a User ID typically contains an e-mail address.
While {{?RFC5322}} and {{?RFC6531}} describe an `addr-spec` as it is used e-mail message headers (us-ascii in the former, and Unicode in the latter), the common OpenPGP User ID convention is somewhat simpler, while still permitting extraction of a valid `addr-spec`.
An e-mail-oriented OpenPGP implementation that follows this simpler convention is more likely to be interoperable with Transferable Public Keys found in the wild.

In particular, the common convention for an OpenPGP User ID related to e-mail can be described with the following ABNF (see {{!RFC5234}}), which uses the Unicode-augmented definitions of `atext` and `dot-atom-text` found in {{!RFC6532}}:

    openpgp-addr-spec            = dot-atom-text "@" dot-atom-text

    openpgp-email-prefix-char    = atext / specials / SPACE

    openpgp-email-wrapped-addr   = *openpgp-uid-prefix-char
                                   "<" openpgp-addr-spec ">"

    openpgp-email-uid-convention = openpgp-addr-spec /
                                   openpgp-email-wrapped-addr

Note that any `openpgp-addr-spec` described in the above sequence is also a valid Unicode `addr-spec`.
The only `addr-spec`s not matched are obsolete forms, or those with `CWFS` or `quoted-string` in the local part, or those with domain literals for the domain part.
Using such a non-matching `addr-spec` in an OpenPGP User ID is likely to lead to interoperability problems.

# Internationalized Domain Names

FIXME: if a domain name in the `openpgp-addr-spec` contains non-ASCII characters, will existing implementations accept A-labels?
Or should we encourage standardization on U-labels (see {{?RFC5980}})?

# Example User IDs

## Conventional User IDs

Most tools will work fine with the following User IDs, even though most of them are not technically {{RFC5322}} `name-addr` objects:

- `Alice Jones <alice@example.org>`
- `Alice T. Jones <alice@example.org>`
- `Sean O'Brian <sean@example.org>`
- `Jörg Schmidt <js@example.org>`
- `Mr. Ed, the Talking Horse <ed@example.org>`
- `alice@example.org`

## Examples of Atypical User IDs

The following examples are UTF-8 strings that are valid {{RFC5322}} `name-addr` objects, but would most likely cause interoperability problems if they were used as an OpenPGP User ID:

### RFC 2047 Encoding of non-ASCII Characters

Do not use {{?RFC2047}} encoding:

```
=?utf-8?Q?J=C3=B6rg?= Schmidt <js@example.org>
```

### `quoted-string` Parts

Do not use  {{RFC5322}} `quoted-string` parts:

```
"Sean O'Brian" <sean@example.org>
```

### FWS

Do not use Folding Whitespace (FWS) ({{Section 3.2.2 of RFC5322}}).

```
Alice
  Jones <alice@example.org>
```

It's probably not a good idea to include any control character or whitespace character at all, other than <u> </u> in an OpenPGP User ID.
Do not include newline, carriage returns, tab characters, non-folding characters, byte order marks, etc.

Also, leading or trailing whitespace is likely to cause interoperability failures in any context where the User ID must be cleanly parsed.

### Comments

Avoid Comments ({{Section 3.2.2 of RFC5322}}) with the possible exception of a single comment just before the angle-bracket that delimits the `openpgp-addr-spec`

FIXME: should we discourage comments entirely?
See, for example, the litany of complaints at https://dkg.fifthhorseman.net/blog/openpgp-user-id-comments-considered-harmful.html

```
Alice (the Great) Jones <alice@example.org>
```

or

```
(The Great) Alice Jones <alice@example.org>
```

or

```
Alice Jones <alice@example.org> (The Great)
```

# IANA Considerations

This draft asks IANA to make one change to the OpenPGP protocol group.

In the "Packet Types/Tags registry", update row 13 ("User ID Packet") by adding this document to the "Reference" field.

# Security Considerations

This document describes widespread conventions about User IDs in the OpenPGP ecosystem.

Generating an OpenPGP certificate with a User ID that does not match these conventions may result in security failures when a peer tries to find a certificate but cannot


--- back

# Python Example

The following Python example can be used to parse a conventional OpenPGP User ID:

{: sourcecode-name="openpgp_userid_convention.py"}
~~~ text/x-python
{::include openpgp_userid_convention.py}
~~~

# Document History

RFC Editor Note: Please delete this section before publication.

## Substantive Changes from Origin to draft-ietf-openpgp-userid-conventions-00

- added positive and negative examples
- added Python implementation
- added FIXME about internationalized domain names

## Origin

This was originally discussed on the mailing list at https://mailarchive.ietf.org/arch/msg/openpgp/wNo27-0STfGR9JZSlC7s6OYOJkI and was formulated as a patch to the OpenPGP specification at https://mailarchive.ietf.org/arch/msg/openpgp/wNo27-0STfGR9JZSlC7s6OYOJkI .
