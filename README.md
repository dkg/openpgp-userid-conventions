# An Internet Draft about OpenPGP User ID Conventions

This repository hosts the source for [draft-dkg-openpgp-userid-conventions](https://datatracker.ietf.org/doc/draft-dkg-openpgp-userid-conventions/).
