#!/usr/bin/python3

from typing import Optional, Tuple
import re

def openpgp_userid(test: str) -> Optional[Tuple[str, str]]:
    '''Returns a None if `test` is not a conventional User ID.

    if `test` is a conventional User ID, returns a Tuple containing
    the User ID and the embedded e-mail address.'''

    specials = r'[()<>\[\]:;@\\,."]'
    atext = "[-A-Za-z0-9!#$%&'*+/=?^_`{|}~\x80-\U0010ffff]"
    dot_atom_text = atext + r"+(?:\." + atext + "+)*"
    pgp_addr_spec = dot_atom_text + "@" + dot_atom_text
    pgp_uid_prefix_char = "(?:" + atext + "|" + specials + "| )"
    addr_spec_raw = "(?P<addr_spec_raw>" + pgp_addr_spec + ")"
    addr_spec_wrapped = pgp_uid_prefix_char + \
        "*<(?P<addr_spec_wrapped>" + pgp_addr_spec + ")>"
    pgp_uid_convention = "^(?:" + addr_spec_raw + "|" + \
        addr_spec_wrapped + ")$"

    pgp_uid_convention_re = re.compile(pgp_uid_convention,
                                       re.UNICODE)

    m = pgp_uid_convention_re.search(test)
    if m:
        return (m[0], m['addr_spec_wrapped'] or m['addr_spec_raw'])
    else:
        return None

